local GCGUI = {}
function setColor(pColor)
  local color = {}
  if pColor ~= nil then
    -- If the specified color use 0-255 values,convert it to 0-1 values
    for i, v in ipairs(pColor) do
      if v > 1 then
        v = v / 255
      end
      color[i] = v
    end
  else
    color[1] = 1
    color[2] = 1
    color[3] = 1
    color[4] = 1
  end
  return color
end

---@param pX number
---@param pY number
local function newElement(pX, pY)
  local myElement = {}
  myElement.x = pX
  myElement.y = pY
  myElement.isVisible = false
  function myElement:draw()
    --print("newElement / draw / Not implemented")
  end
  function myElement:update(dt)
    --print("newElement / update / Not implemented")
  end

  ---@param pVisible boolean
  function myElement:setVisible(pVisible)
    self.isVisible = pVisible
  end
  return myElement
end

---@param pX number
---@param pY number
---@param pW number
---@param pH number
function GCGUI.newPanel(pX, pY, pW, pH)
  local myPanel = newElement(pX, pY)
  myPanel.image = nil
  myPanel.width = pW
  myPanel.height = pH
  myPanel.isHover = false
  myPanel.listEvents = {}

  ---@param pImage love.image
  function myPanel:setImage(pImage)
    myPanel.image = pImage
    myPanel.width = pImage:getWidth()
    myPanel.height = pImage:getHeight()
  end

  function myPanel:setEvent(pEventType, pFunction)
    self.listEvents[pEventType] = pFunction
  end

  function myPanel:updatePanel(dt, pX, pY)
    local mouse = {}
    mouse.x, mouse.y = love.mouse.getPosition()
    if mouse.x > pX + self.x and mouse.x < pX + self.x + self.width and mouse.y > pY + self.y and mouse.y < pY + self.y + self.height then
      if not self.isHover then
        self.isHover = true
      end
    else
      if self.isHover then
        self.isHover = false
      end
    end
  end

  function myPanel:update(dt, pX, pY)
    myPanel:updatePanel(dt, pX, pY)
  end

  function myPanel:drawPanel(pX, pY)
    if self.image == nil then
      love.graphics.rectangle("line", pX + self.x + 1, pY + self.y, self.width, self.height)
    else
      love.graphics.draw(self.image, pX + self.x, pY + self.y)
    end
  end
  function myPanel:draw(pX, pY)
    self:drawPanel(pX, pY)
  end
  return myPanel
end

---@param pX number
---@param pY number
---@param pText string
---@param pFont love.font
---@param phAlign string
---@param pvAlign string
---@param pW number
---@param pH number
---@param pColor table
---@return table
function GCGUI.newLabel(pX, pY, pText, pFont, phAlign, pvAlign, pW, pH, pColor)
  local myLabel = GCGUI.newPanel(pX, pY, pW, pH, pColor)
  myLabel.text = nil
  myLabel.hAlign = phAlign
  myLabel.vAlign = pvAlign
  myLabel.showBorder = pShowBorder
  -- Load the  default font if not specified
  if pFont ~= nil then
    myLabel.font = pFont
  else
    myLabel.font = love.graphics.getFont()
  end

  -- Check the validity of the color
  myLabel.color = setColor(pColor)
  local printX, printY
  -- Update label text then rearange it's position
  function myLabel:setText(pNewText)
    if pNewText == self.text then
      return
    end
    self.text = pNewText
    self.textW = self.font:getWidth(self.text)
    self.textH = self.font:getHeight(self.text)

    -- Calculate width if not specified or that the specified width can't contain the text
    if pW == nil or pW < self.textW then
      self.width = self.textW
    end
    -- Calculate width if not specified or that the specified width can't contain the text
    if pH == nil or pH < self.textH then
      self.height = self.textH
    end
    printX, printY = self.x, self.y
    -- Handle horizontal alignment
    if self.hAlign == "center" then
      printX = printX + (self.width - self.textW) / 2
    elseif self.hAlign == "right" then
      printX = printX + (self.width - self.textW)
    end

    -- Handle vertical aligment
    if self.vAlign == "center" then
      printY = printY + (self.height - self.textH) / 2
    elseif self.vAlign == "bottom" then
      printY = printY + (self.height - self.textH)
    end
  end

  myLabel:setText(pText)

  function myLabel:drawLabel(pX, pY)
    love.graphics.print(self.text, pX + printX, pY + printY)
  end

  function myLabel:draw(pX, pY)
    local oldFont = love.graphics.getFont()
    love.graphics.setFont(self.font)
    local oldColor = { love.graphics.getColor() }

    if self.color[4] ~= nil then
      love.graphics.setColor(self.color[1], self.color[2], self.color[3], self.color[4])
    else
      love.graphics.setColor(self.color[1], self.color[2], self.color[3])
    end

    self:drawLabel(pX, pY)
    love.graphics.setFont(oldFont)
    love.graphics.setColor(oldColor[1], oldColor[2], oldColor[3], oldColor[4])
  end
  return myLabel
end

---@param pX number
---@param pY number
---@param pText string
---@param pFont love.font
---@param pW number
---@param pH number
---@param pborder boolean
---@param pColor table
---@return table
function GCGUI.newButton(pX, pY, pW, pH, pText, pFont, pborder, pColor)
  local myButton = GCGUI.newPanel(pX, pY, pW, pH)
  myButton.text = pText
  myButton.label = GCGUI.newLabel(pX, pY, pText, pFont, "center", "center", pW, pH, setColor(pColor))
  myButton.font = pFont
  myButton.border = pborder

  myButton.isPressed = false
  myButton.oldButtonState = false

  ---@param pNewX number
  function myButton:setX(pNewX)
    self.x = pNewX
    self.label.x = pNewX
  end

  ---@param pNewY number
  function myButton:setY(pNewY)
    self.y = pNewY
    self.label.y = pNewY
  end

  ---@param pImgDefault love.image
  ---@param pImgHover love.image
  ---@param pImgPressed love.image
  function myButton:setImages(pImgDefault, pImgHover, pImgPressed)
    myButton.imgDefault = pImgDefault
    myButton.imgHover = pImgHover
    myButton.imgPressed = pImgPressed
    myButton.width = pImgDefault:getWidth()
    myButton.height = pImgDefault:getHeight()
  end

  function myButton:updateButton(dt, pX, pY)
    myButton:updatePanel(dt, pX, pY)

    if self.isHover and love.mouse.isDown(1) and not self.isPressed and not self.oldButtonState then
      self.isPressed = true
      if self.listEvents["pressed"] ~= nil then
        self.listEvents["pressed"]()
      end
    else
      if self.isPressed and not love.mouse.isDown(1) then
        self.isPressed = false
        if self.listEvents["released"] ~= nil then
          self.listEvents["released"]()
        end
      end
    end
    self.oldButtonState = love.mouse.isDown(1)
  end

  function myButton:update(dt,pX,pY)
    self:updateButton(dt,pX,pY)
  end

  function myButton:draw(pX, pY)
    love.graphics.setColor(1, 1, 1)
    if self.isPressed then
      if self.imgPressed == nil then
        if self.border == nil or self.border == true then
          self:drawPanel(pX, pY)
        end
        love.graphics.setColor(setColor({ 255, 255, 255, 50 }))
        love.graphics.rectangle("fill", pX + self.x, pY + self.y, self.width, self.height)
        love.graphics.setColor(setColor({ 255, 255, 255 }))
      else
        love.graphics.draw(self.imgPressed, pX + self.x, pY + self.y)
      end
    elseif self.isHover then
      if self.imgHover == nil then
        if self.border == nil or self.border == true then
          self:drawPanel(pX, pY)
        end
        love.graphics.setColor(setColor({ 255, 255, 255 }))
        love.graphics.rectangle("line", pX + self.x + 2, pY + self.y + 2, self.width - 2, self.height - 4)
      else
        love.graphics.draw(self.imgHover, pX + self.x, pY + self.y)
      end
    else
      if self.imgDefault == nil then
        if self.border == nil or self.border == true then
          self:drawPanel(pX, pY)
        end
      else
        love.graphics.draw(self.imgDefault, pX + self.x, pY + self.y)
      end
    end
    self.label:draw(pX, pY)
  end
  return myButton
end

function GCGUI.newGroup(pX, pY)
  local myGroup = {}
  myGroup.x = pX
  myGroup.y = pY
  myGroup.elements = {}
  function myGroup:addElement(pElement)
    table.insert(self.elements, pElement)
  end

  ---@param pVisible boolean
  function myGroup:setVisible(pVisible)
    for n, v in pairs(myGroup.elements) do
      if v.setVisible ~= nil then
        v:setVisible(pVisible)
      else
        --print("setVisible don't exists")
      end
    end
  end

  function myGroup:updateChildren(dt)
    for n, v in pairs(myGroup.elements) do
      if v.isVisible and v.update ~= nil then
        v:update(dt, self.x, self.y)
      else
        --print("Function update don't exist or element is hidden")
      end
    end
  end

  function myGroup:update(dt)
    self:updateChildren(dt)
  end

  function myGroup:drawChildren()
    love.graphics.push()
    for n, v in pairs(myGroup.elements) do
      if v.isVisible and v.draw ~= nil then
        v:draw(self.x, self.y)
      else
        --print("Function draw don't exist or element is hidden")
      end
    end
    love.graphics.pop()
  end

  function myGroup:draw()
    self:drawChildren()
  end
  return myGroup
end

---@param pX number
---@param pY number
---@param pW number
---@param pH number
---@param pText string
---@param pFont love.font
---@param pWidth number
---@param pHeight number
---@param pColor table
---@return table
function GCGUI.newMenu (pX, pY, pW, pH, pText, pFont, pWidth, pHeight, pColor)
  local myMenu = GCGUI.newGroup(pX, pY)
  myMenu.width = pWidth
  myMenu.height = pHeight
  myMenu.parent = GCGUI.newButton(pX, pY, pW, pH, pText, pFont, false, pColor)

  function onParentReleased()
    myMenu:setVisible(not myMenu.elements[1].isVisible)
  end
  myMenu.parent:setEvent("released", onParentReleased)

  function myMenu:update(dt)
    myMenu.parent:update(dt, self.x, self.y)
    self:updateChildren()

    local mouse = {}
    mouse.x, mouse.y = love.mouse.getPosition()
    if (love.mouse.isDown(1) or love.mouse.isDown(2)) and  myMenu.elements[1].isVisible and not self.parent.isHover then
      local turnOff = false
      if mouse.x < self.x or mouse.y < self.y or self.x + self.width < mouse.x and mouse.y > self.y + self.width then
        turnOff = true
      end
      if turnOff then
        self:setVisible(false)
      end
    end
  end
  function myMenu:draw(dt)
    myMenu.parent:draw(self.x, self.y)
    self:drawChildren(self.x, self.y)
  end
  return myMenu
end

return GCGUI